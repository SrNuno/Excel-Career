# OPENWEBINARS - CARRERA ESPECIALISTA EN MICROSOFT EXCEL
-----------------------------------------------------------------------------------------------------------
03/15/2023_00:41 -> Comienzo del primer curso de la carrera: Curso de Excel para principiantes.

03/15/2023_00:42 -> Finalización 1.1 (Curso de Excel para principiantes - Trabajar con archivos en Excel).

03/15/2023_00:52 -> Finalización 1.2 (Curso de Excel para principiantes - Formato de celdas).

03/15/2023_01:13 -> Finalización 1.3 (Curso de Excel para principiantes - Realizar cálculos).
	
	Apartados para terminar el curso: 3

------------------------------------------------------------------------------------------------------------
	
03/17/2023_01:48 -> Finalización 1.4 (Curso de Excel para principiantes - Gráficos).

03/17/2023_02:03 -> Finalización 1.5 Curso de Excel para principiantes - Hojas de cálculo).

03/17/2023_02:12 -> Finalización 1.5 Curso de Excel para principiantes - Diseño de impresión).

03/17/2023_02:34 -> Finalización del Curso de Excel para principiantes.

-------------------------------------------------------------------------------------------------------------

03/20/2023_22:18 -> Comienzo del segundo curso de la carrera: Curso de Excel (Funciones básicas).

03/21/2023_01:07 -> Finalización 2.1 (Curso de Excel: Funciones básicas - Funciones matemáticas).

	Apartados para terminar el curso: 3

------------------------------------------------------------------------------------------------------------

03/22/2023_01:45 -> Finalización 2.2 (Curso de Excel: Funciones básicas - Funciones estadísticas).

	Apartados para terminar el curso: 2

------------------------------------------------------------------------------------------------------------

04/04/2023_17:10 -> Finalización 2.3 (Curso de Excel: Funciones básicas - Funciones de fecha).

	Apartados para terminar el curso: 1
	
------------------------------------------------------------------------------------------------------------	
